    #define 	For(J,R,K) 	for(ll J=R;J<K;++J)
    #define 	Rep(I,N) 	For(I,0,N)
    #include <iostream>
    #include <boost/multiprecision/cpp_int.hpp>
    using namespace std;
    namespace mp = boost::multiprecision;
    typedef long long ll;
    int main()
    {
        mp::int512_t ans;
        ll t;
        cin>>t;
        while(t--)
        {
            ans =1;
            ll n;
            cin>>n;
            Rep(i , n)
            {
                ans*=((2*n)-i);
                ans/=(i+1);
            }
            cout<<ans/(n+1)<<endl;
     
        }
        return 0;
    }
    //cr(2n,n)/(n+1)
//    1 1 2 5 14 42 132 429 1430 4862